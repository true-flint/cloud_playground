#!/bin/bash

export ssh_key_path="${HOME}/.ssh/id_rsa"

export TF_VAR_admin_username=${USER}

if [ ! -e ${ssh_key_path} ]; then
    echo "ssh key is missing - creating"
    mkdir -p $(dirname ${ssh_key_path})
    ssh-keygen -t rsa -N '' -b 4096 -f ${ssh_key_path}
fi

export TF_VAR_admin_key=$(cat "${ssh_key_path}.pub")