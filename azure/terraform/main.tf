provider "azurerm" {}

variable region {}
variable resource_group_name {}
variable admin_username {}
variable admin_key {}

locals {
  images = {
    ubuntu = {
      publisher = "Canonical"
      offer     = "UbuntuServer"
      sku       = "16.04-LTS"
      version   = "latest"
    },
    centos = {
      publisher = "OpenLogic"
      offer = "CentOS"
      sku =  "7.5"
      version = "latest"
    }
  }
}

module "network" {
  source = "./network"
  network_name = "virtual-network"
  subnet_name = "public-network-subnet"
  region = var.region
  resource_group_name = var.resource_group_name
}

module "dns_master_vm" {
  source = "./vm"
  region = var.region
  resource_group_name = var.resource_group_name
  subnet_id = module.network.subnet_id
  vm_name = "dnsserver"
  admin_username = var.admin_username
  admin_key = var.admin_key
  storage_image = local.images["ubuntu"]
  tags = {
    role = "dns-master"
  }
}

module "master_vm" {
  source = "./vm"
  region = var.region
  resource_group_name = var.resource_group_name
  subnet_id = module.network.subnet_id
  vm_name = "salt"
  admin_username = var.admin_username
  admin_key = var.admin_key
  storage_image = local.images["ubuntu"]
  tags = {
    role = "salt-master"
  }
}

module "minion1_vm" {
  source = "./vm"
  region = var.region
  resource_group_name = var.resource_group_name
  subnet_id = module.network.subnet_id
  vm_name = "minion1"
  admin_username = var.admin_username
  admin_key = var.admin_key
  storage_image = local.images["ubuntu"]
  tags = {
    role = "salt-minion"
  }
}

module "minion2_vm" {
  source = "./vm"
  region = var.region
  resource_group_name = var.resource_group_name
  subnet_id = module.network.subnet_id
  vm_name = "minion2"
  admin_username = var.admin_username
  admin_key = var.admin_key
  storage_image = local.images["centos"]
  tags = {
    role = "salt-minion"
  }
}


output public_addresses {
  value = {
    "master_vm" = module.master_vm.public_address,
    "minion1" = module.minion1_vm.public_address,
    "minion2" = module.minion2_vm.public_address,
    "dns_server" = module.dns_master_vm.public_address
  }
}