resource "azurerm_virtual_network" "main" {
  name                = var.network_name
  address_space       = [var.cidr]
  location            = var.region
  resource_group_name = var.resource_group_name
}

resource "azurerm_subnet" "internal" {
  name                 = var.subnet_name
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefix       = var.subnet_cidr
}
