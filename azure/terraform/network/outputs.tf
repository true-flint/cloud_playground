output "subnet_id" {
  value = azurerm_subnet.internal.id
  description = "ID of subnet resource"
}
