variable network_name {
  type        = string
  description = "Name of virtual network resource"
}

variable cidr {
  type        = string
  default     = "10.0.0.0/8"
  description = "Virtual network cidr"
}

variable region {
  type        = string
  description = "Region to create resources in"
}

variable resource_group_name {
  type        = string
  description = "Name of resource group to assign resources to"
}

variable subnet_name {
    type = string
    description = "Name of subnet resource"
}

variable subnet_cidr {
    type = string
    description = "Subnet cidr"
    default = "10.1.0.0/16"
}
