resource "azurerm_public_ip" "main" {
  name                = "${var.vm_name}-public-address"
  location            = var.region
  resource_group_name = var.resource_group_name
  allocation_method   = "Static"
}


resource "azurerm_network_interface" "main" {
  name                = "${var.vm_name}-nic"
  location            = var.region
  resource_group_name = var.resource_group_name

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = var.subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.main.id
  }
}

resource "azurerm_virtual_machine" "main" {
  name                  = var.vm_name
  location              = var.region
  resource_group_name   = var.resource_group_name
  network_interface_ids = [azurerm_network_interface.main.id]
  vm_size               = "Standard_DS1_v2"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = var.storage_image.publisher
    offer     = var.storage_image.offer
    sku       = var.storage_image.sku
    version   = var.storage_image.version
  }
  storage_os_disk {
    name              = "${var.vm_name}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name = var.vm_name
    admin_username = var.admin_username
  }

  os_profile_linux_config  {
    disable_password_authentication = true
    ssh_keys {
      path = "/home/${var.admin_username}/.ssh/authorized_keys"
      key_data = var.admin_key
    }
  }

  tags = var.tags
}
