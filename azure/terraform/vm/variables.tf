variable subnet_id {
  type        = string
  description = "ID of subnet resource"
}

variable vm_name {
  type = string
  description = "Name of VM resource"
}

variable admin_username {
  type = string
  description = "Admin username"
}

variable admin_key {
  type = string
  description = "Admin ssh publickey"
}

variable region {
  type = string
}

variable resource_group_name {
  type = string
}

variable storage_image {
  type = object(
    {
      publisher = string
      offer     = string
      sku       = string
      version   = string
    }
  )
}

variable tags {
  type = map
}